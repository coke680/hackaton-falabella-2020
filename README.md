** E-commerce - Similar products **

Description: Build an application which can suggest the products similar to an input product. The similarity will be determined basis the various attributes and characteristics of the product catalog shared.

The solution must: Suggest upto the configured number of similar products for an input product. Solution must be able to recommend the products using the data provided as part of the problem, using various attributes like Categories, Color, and other attributes available as part of data set. ​Providing recommendations using the product images will be a good addition to the solution​

Evaluation Criteria: Golden set created for few SKUs (~25 SKUs) and the number of matching recommendations to be evaluated using Precision at K (10) algorithm. The methodology used and the scalability of the solution.

** Solution created by DataWizards (ID = 53) using Cosine Similarity**